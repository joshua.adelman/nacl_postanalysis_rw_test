from __future__ import division, print_function; __metaclass__ = type
import os, sys, math, itertools
import numpy
import west
from west import WESTSystem
from westpa.binning import RectilinearBinMapper
import westpa

import logging
log = logging.getLogger(__name__)
log.debug('loading module %r' % __name__)

class System(WESTSystem):
    def initialize(self):
        self.pcoord_ndim = 2
        self.pcoord_len = 101
        self.pcoord_dtype = numpy.float32
        self.dist_binbounds = [0.0,2.80,2.88,3.00,3.10,3.29,3.79,3.94,4.12,4.39,5.43,5.90,6.90,7.90,8.90,9.90,10.90,11.90,12.90,13.90,14.98,15.90,float('inf')]
        self.color_binbounds = [0,1,2,float('inf')]
        self.unknown_state = 2
        self.bin_mapper = RectilinearBinMapper([self.dist_binbounds, self.color_binbounds])
        self.bin_target_counts = numpy.empty((self.bin_mapper.nbins,), numpy.int)
        self.bin_target_counts[...] = 25
    
def coord_loader(fieldname, coord_file, segment, single_point=False):
    coord_raw = numpy.loadtxt(coord_file, dtype=numpy.float32) 
    color_bins = [(0.0,2.8),(14.98,float('inf'))]
    unknown_state = 2
    new_walker = 0
    system = westpa.rc.get_system_driver()
    #print(system.pcoord_ndim)

    try:
        npts = len(coord_raw)
    except(TypeError):
        npts = 1
        new_walker = 1

    coords = numpy.empty((npts), numpy.float32)
    colors = numpy.empty((npts), numpy.float32)
    if new_walker == 1:
        colors[:] = unknown_state
        for istate,state_tuple in enumerate(color_bins):
            if coord_raw >= state_tuple[0] and coord_raw < state_tuple[1]:
                colors[:] = istate
    else:
        colors[:] = segment.pcoord[0][1]
    if new_walker == 1:
        coords[0] = coord_raw
    else:
        coords[:] = coord_raw
    for istate,state_tuple in enumerate(color_bins):
        if coords[-1] >= state_tuple[0] and coords[-1] < state_tuple[1]:
            colors[-1] = istate
    
    if new_walker == 1:
        segment.pcoord = numpy.hstack((coords[:],colors[:]))
    else:
        segment.pcoord = numpy.swapaxes(numpy.vstack((coords[:],colors[:])), 0, 1)

    
    
    
    
    
