import h5py
import argparse
import os


def run(args):
    stride = args.stride


    with h5py.File(args.h5i_name, 'r') as h5i, h5py.File(args.h5o_name, 'w') as h5o:

        for k in h5i.attrs:
            h5o.attrs[k] = h5i.attrs[k]

        for grp_name in h5i.keys():
            if grp_name != 'iterations':
                print grp_name
                h5i.copy(grp_name, h5o, name=grp_name)

        data = []

        def get_names(name, obj):
            data.append(name)

        for iter_name in h5i['iterations'].keys():
            print iter_name
            for x in h5i['iterations'][iter_name].keys():
                if x != 'pcoord':
                    path = os.path.join('iterations', iter_name, x)
                    h5i.copy(path, h5o, name=path)
                else:
                    path = os.path.join('iterations', iter_name, 'pcoord')
                    pc_full = h5i[path][:]

                    assert (pc_full.shape[1] - 1) % stride == 0
                    pc_subsampl = pc_full[:,::stride,:]


                    h5o.create_dataset(path, data=pc_subsampl)


if __name__== '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', required=True, dest='h5i_name', help='Original h5 file')
    parser.add_argument('-o', required=True, dest='h5o_name', help='Subsampled h5file')
    parser.add_argument('-s', default=1, type=int, dest='stride', help='Stride between timepoints')

    args = parser.parse_args()

    run(args)
