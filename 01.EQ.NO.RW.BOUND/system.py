from __future__ import division, print_function; __metaclass__ = type
import os, sys, math, itertools
import numpy
import west
from west import WESTSystem
from westpa.binning import RectilinearBinMapper

import logging
log = logging.getLogger(__name__)
log.debug('loading module %r' % __name__)

class System(WESTSystem):
    def initialize(self):
        self.pcoord_ndim = 1
        self.pcoord_len = 101
        self.pcoord_dtype = numpy.float32
        binbounds = [0.0,2.80,2.88,3.00,3.10,3.29,3.79,3.94,4.12,4.39,5.43,5.90,6.90,7.90,8.90,9.90,10.90,11.90,12.90,13.90,14.90,15.90,float('inf')]
#        binbounds = [0.0,2.80,2.88,3.00,3.10,3.29,3.79,3.94,4.12,4.39,4.59,4.8,5.0,5.25,5.43,5.7,5.90,6.5,6.90,7.5,7.6,7.7,7.8,7.90,8.0,8.1,8.2,8.3,8.4,8.5,8.6,8.7,8.8,8.90,9.0,9.2,9.4,9.6,9.7,9.90,10.90,11.90,12.90,13.90,14.90,15.90,float('inf')]
#        binbounds = [0.0,2.80,2.88,3.00,3.10,3.29,3.79,3.94] + [4.0+0.1*i for i in xrange(0,79)] + [11.90,12.90,13.90,14.90,15.90,float('inf')]
#        binbounds = [0.0,2.80,2.88,3.00,3.10,3.29,3.79,3.94,4.12,4.39,5.43,5.90,6.90] + [7.0+0.01*i for i in xrange(0,479)] + [12.90,13.90,14.90,15.90,float('inf')]
        self.bin_mapper = RectilinearBinMapper([binbounds])
        self.bin_target_counts = numpy.empty((self.bin_mapper.nbins,), numpy.int)
        self.bin_target_counts[...] = 50

def coord_loader(fieldname, coord_file, segment, single_point=False):
    coord_raw = numpy.loadtxt(coord_file, dtype=numpy.float32) 

    npts = len(coord_raw)
    assert coord_raw.shape[1] % 3 == 0
    ngrps = coord_raw.shape[1] // 3

    coords = numpy.empty((ngrps, npts, 3), numpy.float32)
    for igroup in xrange(ngrps):
        for idim in xrange(3):
            coords[igroup,:,idim] = coord_raw[:,igroup*3+idim]
    # convert to Angstroms
    coords *= 10

    segment.data[fieldname] = coords
    

